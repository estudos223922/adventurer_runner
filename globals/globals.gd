extends Node

const SAVE_CONFIG_PATH = "user://config.cfg"
const SAVE_SCORE_PATH = "user://records.save"

var score : float = 0
var gem : float = 0
var gameover : bool = false

var config = ConfigFile.new()
var loaded_config = config.load(SAVE_CONFIG_PATH)

var best_score : float = 0
var best_gem_score : float = 0

var _file := File.new()


func save_exists() -> bool:
	return _file.file_exists(SAVE_SCORE_PATH)


func try_open_file(method) -> void:
	var error := _file.open(SAVE_SCORE_PATH, method)
	if error != OK:
		printerr("Could not open the file %s. Aborting save operation. Error code: %s" %
		[SAVE_SCORE_PATH, error])
		return


func write_save_scores() -> void:
	_file.open(SAVE_SCORE_PATH, File.WRITE)

	var data := {
		"best_score" : best_score,
		"best_gem_score" : best_gem_score
	}
	
	var json_string := JSON.print(data)
	_file.store_string(json_string)
	_file.close()


func load_save_scores() -> void:
	
	if save_exists():
		_file.open(SAVE_SCORE_PATH, File.READ)

		var content := _file.get_as_text()
		_file.close()
		
		var data : Dictionary = JSON.parse(content).result
		best_score = data.best_score
		best_gem_score = data.best_gem_score


func save_config(section, key, value) -> void:
	config.set_value(section, key, value)
	config.save(SAVE_CONFIG_PATH)


func load_config(section, key, default=null) -> ConfigFile:
	return config.get_value(section, key, default)


func reset_game() -> void:
	gameover = false
	score = 0
	gem = 0


func get_score_number() -> String:
	return str(int(score))


func get_gem_score_number() -> String:
	return str(int(gem))


func get_score_text() -> String:
	return tr("KEY_SCORE") + ": " + str(int(score))


func get_record_text() -> String:
	return str(best_score)


func get_gem_record_text() -> String:
	return str(best_gem_score)


func pause_game(parent, child):
	if gameover == false:
		get_tree().paused = true
		parent.add_child(child)


func resume_game(parent, child):
	get_tree().paused = false
	parent.remove_child(child)


func update_scores() -> void:
	if best_score < score:
		best_score = score

	if best_gem_score < gem:
			best_gem_score = gem

	write_save_scores()


func change_language(locale) -> void:
	TranslationServer.set_locale(locale)


func play_select(audio) -> void:
	audio.play()
	yield(audio, "finished")


func _ready():
	load_save_scores()

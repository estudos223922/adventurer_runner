# Adventurer Runner

![screens](./assets/screens/screens.webp)

This is my version of [Uniday Studio](https://www.unidaystudio.com.br/) - Godot Masters course lesson.
I challenged myself to improve this project, adding some extra stuff, such as:

- Localization;
- How to play scene;
- Pause scene;
- Confirm exit on Main scene;
- Extra actions (slide, shoot);
- Collect gems;
- Add enemy;
- Add particles;
- Save config file;
- Save scores as JSON file;

## ART ASSETS

Sprites - [Game Art 2D - Freebies](https://www.gameart2d.com/freebies.html)  
Audio - [Open Game Art](https://opengameart.org/)  
Vignette shader - [Godot Engine](https://github.com/godotengine/godot-demo-projects/tree/master/2d/screen_space_shaders/shaders)  
Font - [Nova Mono](https://www.1001freefonts.com/d/5945/nova-mono.zip)  

## TODO

- [ ] Add settings menu;
- [ ] Improve user interface experience;


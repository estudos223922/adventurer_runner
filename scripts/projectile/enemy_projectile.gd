extends Area2D

export var velocity : float = 20


func _process(delta):
	if Globals.gameover == false:
		self.position.x -= velocity
		if self.position.x <= -150:
			self.queue_free()
 

func _on_Projectile_body_entered(body):
	if body.name == "Player":
		Globals.gameover = true
		Globals.update_scores()

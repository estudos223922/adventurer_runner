extends Area2D

export var velocity : float = 20


func _process(delta):
	if Globals.gameover == false:
		self.position.x += velocity
		if (self.position.x >= get_viewport().size.x - 50):
			self.queue_free()


func _on_Projectile_body_entered(body):
	if "Enemy" in body.name:
		Globals.play_select($Target)
		body.queue_free()
		queue_free()

extends Area2D

export var velocity : float = 1.0

var scored : bool = false


func _process(delta):
	if Globals.gameover == false:
		self.position.x -= velocity
		if self.position.x <= -150:
			self.queue_free()


func _on_Gem_body_entered(body):
	if body.name == "Player":
		$AudioStreamPlayer2D.play(0)
		Globals.gem += 1
		if Globals.best_gem_score < Globals.gem:
			Globals.best_gem_score = Globals.gem
			Globals.write_save_scores()
		$AnimationPlayer.play("take")

extends Control


func _on_BTNRestart_pressed():
	Globals.play_select($AudioSelect)
	Globals.reset_game()
	get_tree().reload_current_scene()


func _on_BTNMainMenu_pressed():
	Globals.play_select($AudioSelect)
	Globals.reset_game()
	get_tree().change_scene("res://scenes/MainMenu.tscn")


func _on_BTNRestart_mouse_entered():
	$AudioFocus.play(0)


func _on_BTNMainMenu_mouse_entered():
	$AudioFocus.play(0)

extends Control


func _ready():
	$"%BTNRecord".text = Globals.get_record_text()
	$"%BTNGemRecord".text = Globals.get_gem_record_text()
	set_disable_locale_buttons()


func set_disable_locale_buttons():
	var locale = Globals.load_config("locale", "lang")
	if locale:
		TranslationServer.set_locale(locale)
	else:
		locale = TranslationServer.get_locale()

	if locale == "pt_BR":
		$"%BTNBr".disabled = true
		$"%BTNUk".disabled = false
	else:
		$"%BTNBr".disabled = false
		$"%BTNUk".disabled = true


func _on_BtnYes_mouse_entered():
	$AudioFocus.play()


func _on_BtnNo_mouse_entered():
	$AudioFocus.play()


func _on_BtnQuit_mouse_entered():
	$AudioFocus.play()


func _on_BTNStart_pressed():
	Globals.play_select($AudioSelect)
	get_tree().change_scene("res://scenes/HowTo.tscn")


func _on_BTNQuit_pressed():
	Globals.play_select($AudioSelect)
	$DialogConfirm.visible = true


func _on_BTNStart_mouse_entered():
	$AudioFocus.play()


func _on_BTNQuit_mouse_entered():
	$AudioFocus.play()


func _on_BTNNo_pressed():
	Globals.play_select($AudioSelect)
	$DialogConfirm.visible = false


func _on_BTNYes_pressed():
	Globals.play_select($AudioSelect)
	get_tree().quit()


func _on_BTNUk_pressed():
	Globals.play_select($AudioSelect)
	Globals.change_language("en")
	Globals.save_config("locale", "lang", "en")
	set_disable_locale_buttons()


func _on_BTNBr_pressed():
	Globals.play_select($AudioSelect)
	Globals.change_language("pt_BR")
	Globals.save_config("locale", "lang", "pt_BR")
	set_disable_locale_buttons()


func _on_MainMenu_mouse_entered():
	$AudioFocus.play()


func _on_BTNUk_mouse_entered():
	$AudioFocus.play()

extends Control

var _menu : bool


func _process(delta):
	_menu = Input.is_action_just_pressed("menu")
	if _menu:
		Globals.resume_game(get_parent(), self)


func _on_BTNMainMenu_pressed():
	get_tree().paused = false
	Globals.play_select($AudioSelect)
	get_tree().change_scene("res://scenes/MainMenu.tscn")


func _on_BTNResume_pressed():
	Globals.play_select($AudioSelect)
	Globals.resume_game(get_parent(), self)


func _on_BTNResume_mouse_entered():
	$AudioFocus.play(0)


func _on_BTNMainMenu_mouse_entered():
	$AudioFocus.play(0)

extends Control

const _gameover_scene = preload("res://scenes/GameOver.tscn")
const _gamepaused_scene = preload("res://scenes/GamePaused.tscn")

var _is_animated : bool = false
var _gameover_scene_isntance = _gameover_scene.instance()
var _gamepaused_scene_isntance = _gamepaused_scene.instance()
var _menu : bool

func _process(delta):
	$"%Score".text = Globals.get_score_number()
	$"%GemScore".text = Globals.get_gem_score_number()

	_menu = Input.is_action_just_pressed("menu")

	if _menu:
		_gamepaused_scene_isntance.rect_size = Vector2(1280, 720)
		Globals.pause_game(get_parent(), _gamepaused_scene_isntance)

	if Globals.gameover:
		if _is_animated == false:
			_gameover_scene_isntance.rect_size = Vector2(1280, 720)
			get_parent().add_child(_gameover_scene_isntance)
			$"../AudioStreamPlayer2D".stop()
			_gameover_scene_isntance.find_node("AudioStreamPlayer2D").play(0)
			_gameover_scene_isntance.find_node("AnimationPlayer").play("gameover")
			_gameover_scene_isntance.find_node("BTNScore").text = Globals.get_score_number()
			_gameover_scene_isntance.find_node("BTNGemScore").text = Globals.get_gem_score_number()
			
			_is_animated = true

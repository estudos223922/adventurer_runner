extends Control


func _process(delta):
	$"%AnimationJump".play("how_to_jump")
	$"%AnimationSlide".play("how_to_slide")
	$"%AnimationShoot".play("how_to_shoot")


func _on_BTNLetsGo_pressed():
	Globals.play_select($AudioSelect)
	get_tree().change_scene("res://scenes/PrincipalScene.tscn")


func _on_BTNLetsGo_mouse_entered():
	$AudioFocus.play(0)

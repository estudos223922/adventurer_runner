extends KinematicBody2D

const _bullet = preload("res://scenes/Projectile.tscn")

export var jump_strength : int = 1000
export var gravity : Vector2 = Vector2(0, 50)
export var limit_sliding : float = 0.4

var _velocity : Vector2
var _jump : bool
var _slide : bool
var _slide_release : bool
var _shoot : bool
var _shooting : bool = false
var _time_sliding : float = 0
var _collision_box : RectangleShape2D = RectangleShape2D.new()
var _is_dead : bool = false
var _slide_play : bool = false
var _bullet_instance : Area2D = null


func change_collision_shape(sliding) -> void:
	var _collision : CollisionShape2D = self.get_node("CollisionShape2D")
	var _cy = 96
	_collision_box.extents.x = 55
	_collision_box.extents.y = _cy
	if sliding == true and _time_sliding < limit_sliding:
		_collision_box.extents.y = 65
	_collision.shape = _collision_box


func detect_jump() -> void:
	if self.is_on_floor():
		if _jump == true:
			$"%AnimationPlayer".play("jump")
			_velocity.y =  jump_strength * -1
			$Jump.play(0)
		else:
			$AnimationPlayer.play("run")


func detect_slide(delta) -> void:
	if self.is_on_floor():
		if _slide_release == true:
			_slide = false
			_time_sliding = 0
			_slide_play = false
		if _slide == true and _jump == false:
			_time_sliding += delta
			if _time_sliding < limit_sliding:
				$AnimationPlayer.play("slide")
				if _slide_play == false:
					$Slide.play(0)
					_slide_play = true
		change_collision_shape(_slide)


func detect_shoot(bullet) -> void:
	if self.is_on_floor():
		if _shoot == true:
			$AnimationPlayer.play("shoot")
			$Shoot.play(0)
			_bullet_instance = _bullet.instance()
			_bullet_instance.position = Vector2(self.position.x + 150, self.position.y)
			get_parent().add_child(_bullet_instance)


func _process(delta):
	if Globals.gameover == false:
		_jump = Input.is_action_just_pressed("jump")
		_slide = Input.is_action_pressed("slide")
		_slide_release = Input.is_action_just_released("slide")
		_shoot = Input.is_action_just_pressed("shoot")
		_velocity += gravity
		detect_jump()
		detect_slide(delta)
		detect_shoot(_bullet_instance)
		self.move_and_slide(_velocity, Vector2.UP)
	else:
		if not _is_dead:
			$AnimationPlayer.play("dead")
			_is_dead = true

extends Area2D

export var velocity : float = 1.0

var scored : bool = false

func _process(delta):
	if Globals.gameover == false:
		self.position.x -= velocity
		if self.position.x <= -150:
			self.queue_free()

		var player = get_parent().find_node("Player")
		if self.position.x < player.position.x and scored == false:
				Globals.score += 1
				scored = true


func _on_Obstacle_body_entered(body):
	if body.name == "Player":
		Globals.gameover = true
		Globals.update_scores()

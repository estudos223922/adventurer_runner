extends Position2D

const _obstacle = preload("res://scenes/Obstacle.tscn")
const _gem = preload("res://scenes/Gem.tscn")
const _enemy = preload("res://scenes/Enemy.tscn")

export var min_time_spawn : int = 2
export var max_time_spawn : int = 5


class ObstacleObject:

	var _object
	var _sprite
	var _collision


	func _init(instance):
		_object = instance
		_sprite = _object.get_node("Sprite")
		_collision = _object.get_node("CollisionShape2D")


	func draw_collision(dimension=null, position=null) -> void:
		var _collision_box : RectangleShape2D = RectangleShape2D.new()

		_collision_box.extents.x = _sprite.texture.get_size().x / 2
		_collision_box.extents.y = _sprite.texture.get_size().y / 2
		_collision.shape = _collision_box
		_collision.position.y = (_sprite.texture.get_size().y / 2) * -1

		if dimension:
			_collision_box.extents.x = dimension.x
			_collision_box.extents.y = dimension.y

		if position:
			_collision.position.x = position.x
			_collision.position.y = position.y


	func draw_obstacle(texture, scale=Vector2(1, 1), dimension=null, position=null) -> ObstacleObject:
		_sprite.texture = texture
		_sprite.offset.y = (_sprite.texture.get_size().y / 2) * -1
		_sprite.scale = scale
		draw_collision(dimension, position)
		return _object


	func draw_cactus() -> ObstacleObject:
		var cactus = randi() % 3
		var texture : StreamTexture
		match cactus:
			0:
				texture = preload("res://assets/objects/Cactus (3).png")
			1:
				texture = preload("res://assets/objects/Cactus (1).png")
			2:
				texture = preload("res://assets/objects/Cactus (2).png")
		return draw_obstacle(texture)


	func draw_stone() -> ObstacleObject:
		var texture = preload("res://assets/objects/Stone.png")
		return draw_obstacle(texture)


	func draw_sign() -> ObstacleObject:
		var texture = preload("res://assets/objects/SignArrow.png")
		return draw_obstacle(texture, Vector2(1.5, 3), Vector2(50, 35), Vector2(-20, -185))


	func draw_random():
		randomize()
		var option = randi() % 3
		match option:
			0:
				return draw_cactus()
			1:
				return draw_stone()
			2:
				return draw_sign()


func draw_gem() -> void:
	var _gem_instance = _gem.instance()
	_gem_instance.position = Vector2(self.position.x, self.position.y - rand_range(50, 300))
	get_parent().add_child(_gem_instance)


func draw_enemy() -> void:
	var _enemy_isntance = _enemy.instance()
	_enemy_isntance.position = Vector2(self.position.x, self.position.y - 100)
	get_parent().add_child(_enemy_isntance)


func _on_Timer_timeout():
	if Globals.gameover == false:
		randomize()
		$"../Timer".wait_time = rand_range(min_time_spawn, max_time_spawn)
		draw_gem()
		if randi() % 100 < 25:
			draw_enemy()
		else:
			var instance = _obstacle.instance()
			var obstacle = ObstacleObject.new(instance)
			var obj = obstacle.draw_random()
			obj.position = self.position
			get_parent().add_child(obj)

extends KinematicBody2D

const _projectile = preload("res://scenes/EnemyProjectile.tscn")

export var velocity : float = 10
export var min_time : float = 0
export var max_time : float = 2
export var player_offset_hit : float = 150

var scored : bool = false
var _bullet : Node2D
var _shooting : bool = false
var _player : KinematicBody2D


func _ready():
	randomize()
	_player = get_parent().get_node("Player")


func shoot():
	if _shooting == false:
		_bullet = _projectile.instance()
		_bullet.position = Vector2(self.position.x - 150, self.position.y - 50)
		get_parent().add_child(_bullet)
		$AnimationPlayer.play("enemy_shoot")
		$"%AudioStreamPlayer2D".play(0)
		_shooting = true


func _process(delta):
	if Globals.gameover == false:
		self.position.x -= velocity
		if self.position.x <= get_viewport().size.x:
			shoot()

		if self.position.x <= -150:
			self.queue_free()

		if self.position.x <= _player.position.x + player_offset_hit:
			Globals.gameover = true
			Globals.write_save_scores()

		var player = get_parent().find_node("Player")
		if self.position.x < player.position.x and scored == false:
				Globals.score += 1
				scored = true

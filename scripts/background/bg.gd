extends Sprite

export var velocity : float = 0.0
var displacement : float = 0.0

var width : int
var height : int


func _ready():
	height = get_viewport().size.y
	width = get_viewport().size.x
	self.region_rect.size.x = width


func _process(delta):
	if Globals.gameover == false:
		displacement += velocity
		self.region_rect.position = Vector2(displacement, 0)
		if displacement >= width:
			displacement = 0.0
